const Sequelize = require('sequelize')

const Database = new Sequelize({
  dialect: 'postgres',
  database: 'db_user_mgt',
  username: 'postgres',
  password: '86da9113ab4a4199b1e6218af18d117a',
})

// check database connections
async function checkConnection() {
  console.log('trying to connect postgre database...')
  try {
    await Database.authenticate()
    console.log('Database connection has been established successfully.')
  } catch (error) {
    console.error('Unable to connect to the database ', error)
    throw error
  }
}

module.exports = {
  Database,
  checkConnection,
}
